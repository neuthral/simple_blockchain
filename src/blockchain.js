"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var crypto = require('crypto');
const block_1 = __importDefault(require("./block"));
class Blockchain {
    constructor(genesisBlock) {
        this.blocks = [];
        this.difficulty = 1;
        this.addBlock(genesisBlock);
    }
    addBlock(block) {
        // if genesis block
        if (this.blocks.length === 0) {
            block.previousHash = "0000000000";
            block.hash = this.generateHash(block);
        }
        this.blocks = [...this.blocks, block];
    }
    getPreviousBlock() {
        return this.blocks[this.blocks.length - 1];
    }
    getNextBlock(transactions) {
        let block = new block_1.default();
        transactions.map(function (t) {
            block.addTransaction(t);
        });
        let previousBlock = this.getPreviousBlock();
        block.index = this.blocks.length;
        block.previousHash = previousBlock.hash;
        block.hash = this.generateHash(block);
        return block;
    }
    generateHash(block) {
        let hash = crypto.createHash('sha256').digest("hex");
        // mining
        while (!hash.startsWith(Array(this.difficulty + 1).join('0'))) {
            block.nonce += 1;
            hash = crypto.createHash('sha256').update(block.key).digest("hex");
            console.log(block.nonce, hash);
        }
        this.difficulty += 1;
        return hash;
    }
}
exports.default = Blockchain;
