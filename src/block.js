"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Block {
    constructor(index = 0, hash = '', previousHash = '', nonce = 0, transactions = []) {
        this.index = index;
        this.hash = hash;
        this.previousHash = previousHash;
        this.nonce = nonce;
        this.transactions = transactions;
    }
    get key() {
        return JSON.stringify(this.transactions) + this.index + this.previousHash + this.nonce;
    }
    addTransaction(transaction) {
        this.transactions = [...this.transactions, transaction];
    }
}
exports.default = Block;
