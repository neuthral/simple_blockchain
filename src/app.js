"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// external library
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
// Our classes
const blockchain_1 = __importDefault(require("./blockchain"));
const block_1 = __importDefault(require("./block"));
const transaction_1 = __importDefault(require("./transaction"));
// Setup express REST API
const app = express_1.default();
app.use(body_parser_1.default.json());
// Initialize Blockchain
// create a genesis block
let genesisBlock = new block_1.default();
// initialize blockchain with genesis block
let bc = new blockchain_1.default(genesisBlock);
// Transactions
let transactions = [];
app.get('/', function (req, res) {
    res.json(bc.blocks);
});
app.post('/transaction', function (req, res) {
    let { from, to, amount } = req.body;
    // create a transaction
    let t = new transaction_1.default(from, to, amount);
    transactions = [...transactions, t];
    res.json(t);
});
app.get('/mine', function (req, res) {
    console.time('newB');
    // mining part
    let newB = bc.getNextBlock(transactions);
    transactions = [];
    bc.addBlock(newB);
    res.json(bc);
    console.timeEnd('newB');
});
// Start the server at port 3000
app.listen(3000, function () {
    console.log('post started at 3000');
});
