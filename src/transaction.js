"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Transaction {
    constructor(from, to, amount) {
        this.from = from;
        this.to = to;
        this.amount = amount;
    }
}
exports.default = Transaction;
